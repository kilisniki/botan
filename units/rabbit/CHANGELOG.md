# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.1.0](https://gitlab.com/kilisniki/botan/compare/v1.1.0-beta...v1.1.0) (2022-04-15)


### Features

* refactoring ([92fa46a](https://gitlab.com/kilisniki/botan/commit/92fa46a2ff8d5e87ba329cf457a3cfb71139fb85))


### Bug Fixes

* lint ([8d0f4a2](https://gitlab.com/kilisniki/botan/commit/8d0f4a24e8f4d103361b7c9abd7ec000f82fd971))

### [1.0.5](https://gitlab.com/kilisniki/botan/compare/v1.0.4...v1.0.5) (2022-04-14)

### [1.0.4](https://gitlab.com/kilisniki/botan/compare/v1.1.0...v1.0.4) (2022-04-14)
