import * as amqp from 'amqplib';

export class Producer {
  private timerId: NodeJS.Timer;

  constructor(
    private readonly connection: amqp.Connection,
    public readonly queue: string,
  ) {}

  public async startProducing() {
    const channel = await this.connection.createChannel();
    await channel.assertQueue(this.queue, {
      durable: false,
    });

    console.log(' [P] Start producing...');

    let messageId = 0;
    this.timerId = setInterval(() => {
      const message = ` [M] Message ${++messageId}`;
      console.log(' [P] Send new message ', messageId);
      channel.sendToQueue(this.queue, Buffer.from(message));
    }, 1000);
  }
}
