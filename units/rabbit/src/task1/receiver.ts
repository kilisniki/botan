import * as amqp from 'amqplib';

export class Receiver {
  constructor(
    private readonly connection: amqp.Connection,
    public readonly queue: string,
    public readonly id: number,
  ) {}

  public async startReceive() {
    const channel = await this.connection.createChannel();
    console.log(` [R${this.id}] Waiting messages...`);

    await channel.consume(
      this.queue,
      (msg) => {
        console.log(` [R${this.id}] Received msg: ${msg.content.toString()}`);
      },
      { noAck: true },
    );
  }
}
