import * as amqp from 'amqplib';
import { Producer } from './producer';
import { Receiver } from './receiver';

async function main() {
  const connection = await amqp.connect('amqp://guest:guest@localhost:5672');
  const queue = 'task1';

  const producer = new Producer(connection, queue);
  const receiver1 = new Receiver(connection, queue, 1);
  const receiver2 = new Receiver(connection, queue, 2);

  await producer.startProducing();
  // Будут получать сообщения по очереди
  await receiver1.startReceive();
  await receiver2.startReceive();
}

main()
  .then()
  .catch((e) => console.log('Error', e));
