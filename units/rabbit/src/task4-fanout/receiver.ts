import * as amqp from 'amqplib';

export class Receiver {
  constructor(
    private readonly connection: amqp.Connection,
    public queue: string,
    public readonly exchange: string,
    public readonly id: number,
    public readonly prefetch: number,
  ) {}

  public async startReceive() {
    // !! Обращаем вномание, что мы можем создать очередь вообще с рандомным routingKey
    // Если у нас exchange type === fanout, то реббит не проверяет ключи у сообщений
    this.queue = '12345' + this.id;
    // создаем канал
    const channel = await this.connection.createChannel();
    // устанавливаем префетч
    await channel.prefetch(this.prefetch);
    // создаем очередь
    // обращаем внимание на то, что очередь эксклюзивная, то есть для одного получателя
    // сохраняем объект очереди
    const q = await channel.assertQueue(this.queue, { exclusive: true });
    console.log(q.queue); // id очереди в формате amq.gen-f812wXnI2TJBgOMUrsQTQw
    // вручную биндим очередь и обменник
    // в данном случае this.queue - это routing key
    await channel.bindQueue(q.queue, this.exchange, this.queue);

    console.log(
      ` [R:${this.id}] Waiting messages. With prefetch = ${this.prefetch}`,
    );

    await channel.consume(
      this.queue,
      (msg) => {
        console.log(` [R:${this.id}] Received msg : ${msg.content.toString()}`);

        // Если один из консьюмеров упадет, то сообщение НЕ будет переотправленно другим консьюмерам
        // if (this.id === 1) {
        //   channel.close();
        //   return;
        // }
        setTimeout(() => {
          channel.ack(msg); // Задержка подтверждения сообщений
        }, 1000);
      },
      { noAck: false }, // Должны сами подтвердить сообщение
    );
  }
}
