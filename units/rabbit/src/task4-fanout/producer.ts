import * as amqp from 'amqplib';

export class Producer {
  private timerId: NodeJS.Timer;
  private messageId = 0;

  constructor(
    private readonly connection: amqp.Connection,
    public queue: string,
    public readonly exchange: string,
  ) {}

  public async startProducingToExchange() {
    // !! Обращаем вномание, что мы можем указать очередь вообще с рандомным routingKey
    // Если у нас exchange type === fanout, то реббит не проверяет ключи у сообщений
    this.queue = 'producer' + '123';
    // создаем канал
    const channel = await this.connection.createChannel();

    console.log(' [P] Start producing...');

    this.timerId = setInterval(() => {
      const message = ` [M] for exchange ${++this.messageId}`;
      console.log(' [P] Send new message ', this.messageId);
      // публикуем сообщение не в очередь, а в обменник
      channel.publish(this.exchange, this.queue, Buffer.from(message));
    }, 2000);
  }
}
