import * as amqp from 'amqplib';
import { Producer } from './producer';
import { Receiver } from './receiver';

async function main() {
  const connection = await amqp.connect('amqp://guest:guest@localhost:5672');

  const queue = 'task4-exchange-fanout';
  const exchange = 'task4';

  const channel = await connection.createChannel();
  /*
   * Создаем обменник типа fanout который будет распределять все сообщения во все очереди
   * */
  await channel.assertExchange(exchange, 'fanout', { durable: false });

  const producer = new Producer(connection, queue, exchange);
  const receiver1 = new Receiver(connection, queue, exchange, 1, 1);
  const receiver2 = new Receiver(connection, queue, exchange, 2, 1);

  await producer.startProducingToExchange(); // начнет генерировать сообщения каждые 2 секунды

  await receiver1.startReceive();
  await receiver2.startReceive();
}

main()
  .then()
  .catch((e) => console.log('Error', e));
