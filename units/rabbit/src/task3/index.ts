import * as amqp from 'amqplib';
import { Producer } from './producer';
import { Receiver } from './receiver';

async function main() {
  const connection = await amqp.connect('amqp://guest:guest@localhost:5672');
  const queue = 'task3';

  const channel = await connection.createChannel();
  await channel.assertQueue(queue, {
    durable: false,
  });

  const producer = new Producer(connection, queue);
  const receiver1 = new Receiver(connection, queue, 1, 1);
  const receiver2 = new Receiver(connection, queue, 2, 1);

  await producer.produce(10); // сгенерирует 10 сообщений
  /*
   * Так как prefetch = 1,
   * то очередь начнет разгребаться последовательно между двумя консюмерами
   * */
  await receiver1.startReceive();
  await receiver2.startReceive();
  await producer.produce(10); // сгенерирует 10 сообщений
  await producer.startProducing(); // начнет генерировать сообщения каждые 2 секунды
}

main()
  .then()
  .catch((e) => console.log('Error', e));
