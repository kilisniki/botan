import * as amqp from 'amqplib';

export class Receiver {
  constructor(
    private readonly connection: amqp.Connection,
    public readonly queue: string,
    public readonly id: number,
    public readonly prefetch: number,
  ) {}

  public async startReceive() {
    const channel = await this.connection.createChannel();

    // Говорим сколько сообщений можем иметь в работе неподтвержденных сообщений
    await channel.prefetch(this.prefetch);

    console.log(
      `[R:${this.id}] Waiting messages. With prefetch = ${this.prefetch}`,
    );

    await channel.consume(
      this.queue,
      (msg) => {
        console.log(
          `[R:${this.id}] Received message : ${msg.content.toString()} `,
        );

        // Если консьюмер "упадет", то сообщение будет переотправлено второму консьюмеру
        // if (this.id === 1) {
        //   channel.close();
        //   return;
        // }
        setTimeout(() => {
          channel.ack(msg); // Задержка подтверждения сообщений
        }, 1000);
      },
      { noAck: false }, // Должны сами подтвердить сообщение
    );
  }
}
