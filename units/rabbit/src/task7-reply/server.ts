import * as amqp from 'amqplib';

export class Server {
  constructor(
    private readonly connection: amqp.Connection,
    public queue: string,
    public readonly id: number,
    public readonly prefetch: number,
  ) {}

  public async startReceive() {
    const channel = await this.connection.createChannel();
    await channel.prefetch(this.prefetch);

    console.log(`[Server:${this.id}] Waiting messages.`);

    await channel.consume(
      this.queue,
      async (msg) => {
        console.log(
          `[Server:${this.id}] [correlationId: ${
            msg.properties.correlationId
          }] received message - ${msg.content.toString()} `,
        );

        // Посылаем ответ
        await channel.sendToQueue(
          msg.properties.replyTo,
          Buffer.from(`***reply from: [Server${this.id}]***`),
          { correlationId: msg.properties.correlationId },
        );
        await channel.ack(msg);
      },
      { noAck: false },
    );
  }
}
