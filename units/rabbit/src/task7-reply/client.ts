import * as amqp from 'amqplib';

export class Client {
  private timerId: NodeJS.Timer;
  private messageId = 0;

  constructor(
    private readonly connection: amqp.Connection,
    public serverQueue: string,
  ) {}

  public async startProducing() {
    const channel = await this.connection.createChannel();

    // создаем эксклюзивную очередь для получения ответов
    const clientQueue = await channel.assertQueue('', { exclusive: true });
    // Этот ключ будет посылаться при запросе, а сервер будет знать куда посылать ответ
    console.log(' [Client] queue: ', clientQueue.queue);

    console.log(' [Client] Start producing...');

    this.timerId = setInterval(async () => {
      this.messageId++;
      const message = ` [M:${this.messageId}]`;
      console.log(` [Client] Send new message - [M:${this.messageId}]`);

      const correlationId = this.generateUuid();
      await channel.sendToQueue(this.serverQueue, Buffer.from(message), {
        correlationId,
        // Вот здесь случайно обманулся, указал this.serverQueue и ответы получал второй сервер
        // тупая ошибка, но имеет место быть
        replyTo: clientQueue.queue,
      });
    }, 2000);

    await channel.consume(clientQueue.queue, (msg) => {
      console.log(
        ` [Client] [correlationId:${
          msg.properties.correlationId
        }] received reply: ${msg.content.toString()}`,
      );
    });
  }

  private generateUuid() {
    return (
      Math.random().toString() +
      Math.random().toString() +
      Math.random().toString()
    );
  }
}
