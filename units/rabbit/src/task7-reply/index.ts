import * as amqp from 'amqplib';
import { Client } from './client';
import { Server } from './server';

async function main() {
  const connection = await amqp.connect('amqp://guest:guest@localhost:5672');

  const queue = 'task7';

  const channel = await connection.createChannel();
  await channel.assertQueue(queue, {
    durable: false,
  });

  const client = new Client(connection, queue);
  const server1 = new Server(connection, queue, 1, 1);
  const server2 = new Server(connection, queue, 2, 1);

  await server1.startReceive();
  await server2.startReceive();

  await client.startProducing(); // начнет генерировать события
}

main()
  .then()
  .catch((e) => console.log('Error', e));
