import * as amqp from 'amqplib';
import { Producer } from './producer';
import { Receiver } from './receiver';

async function main() {
  const connection = await amqp.connect('amqp://guest:guest@localhost:5672');

  /*
   * * (star) can substitute for exactly one word.
   * # (hash) can substitute for zero or more words.
   * */
  const patterns = ['*.black.*', '*.*.white', 'red.#'];
  const exchange = 'task6';

  const channel = await connection.createChannel();
  /*
   * Создаем обменник типа topic который будет распределять сообщения по паттерну
   * */
  await channel.assertExchange(exchange, 'topic', { durable: false });

  const producer = new Producer(connection, patterns, exchange);
  const receiver1 = new Receiver(
    connection,
    [patterns[0], patterns[1]],
    exchange,
    1,
    1,
  );
  const receiver2 = new Receiver(connection, [patterns[2]], exchange, 2, 1);

  await receiver1.startReceive();
  await receiver2.startReceive();

  await producer.produce(); // сгенерирует несколько сообщений
}

main()
  .then()
  .catch((e) => console.log('Error', e));
