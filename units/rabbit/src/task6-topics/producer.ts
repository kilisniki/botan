import * as amqp from 'amqplib';

export class Producer {
  private timerId: NodeJS.Timer;
  private messageId = 0;

  constructor(
    private readonly connection: amqp.Connection,
    public routingKeys: string[],
    public readonly exchange: string,
  ) {}

  public async produce() {
    // создаем канал
    const channel = await this.connection.createChannel();

    const patterns = [
      'в.black.рот',
      'мне.ноги.white',
      'red.лол.кек.чебурек',
      'red.ch',
      'test.white',
      'test2.black',
    ];

    console.log(' [P] Start producing...');

    for (const pattern of patterns) {
      this.messageId++;
      const message = ` [M:${this.messageId}] for pattern ${pattern}`;
      console.log(
        ` [P] Send new message - [M:${this.messageId}] for [${pattern}]`,
      );
      // публикуем сообщение не в очередь, а в обменник с необходимым ключом
      channel.publish(this.exchange, pattern, Buffer.from(message));
    }
  }
}
