import * as amqp from 'amqplib';

export class Receiver {
  constructor(
    private readonly connection: amqp.Connection,
    public routingKeys: string[],
    public readonly exchange: string,
    public readonly id: number,
    public readonly prefetch: number,
  ) {}

  public async startReceive() {
    // создаем канал
    const channel = await this.connection.createChannel();
    // устанавливаем префетч
    await channel.prefetch(this.prefetch);
    // создаем очередь
    // обращаем внимание на то, что очередь эксклюзивная, то есть для одного получателя
    // сохраняем объект очереди
    const q = await channel.assertQueue('', { exclusive: true });
    // вручную циклически биндим очередь и обменник по заданным routingKeys
    for (const key of this.routingKeys) {
      await channel.bindQueue(q.queue, this.exchange, key);
    }

    console.log(
      `[R:${this.id}] Waiting messages. Listening routes: [${this.routingKeys}]`,
    );

    await channel.consume(
      '',
      (msg) => {
        console.log(`[R:${this.id}] Received - ${msg.content.toString()} `);

        setTimeout(() => {
          channel.ack(msg);
        }, 1000);
      },
      { noAck: false },
    );
  }
}
