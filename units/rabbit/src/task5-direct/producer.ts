import * as amqp from 'amqplib';

export class Producer {
  private timerId: NodeJS.Timer;
  private messageId = 0;

  constructor(
    private readonly connection: amqp.Connection,
    public routingKeys: string[],
    public readonly exchange: string,
  ) {}

  public async startProducingToExchange() {
    // создаем канал
    const channel = await this.connection.createChannel();

    console.log(' [P] Start producing...');

    this.timerId = setInterval(() => {
      this.messageId++;
      // По очереди будем добавлять сообщения по каждому роуту
      const currentRoute =
        this.routingKeys[this.messageId % this.routingKeys.length];
      const message = ` [M:${this.messageId}] for route ${currentRoute}`;
      console.log(
        ` [P] Send new message - [M:${this.messageId}] for [${currentRoute}]`,
      );
      // публикуем сообщение не в очередь, а в обменник с необходимым ключом
      channel.publish(this.exchange, currentRoute, Buffer.from(message));
    }, 2000);
  }
}
