import * as amqp from 'amqplib';
import { Producer } from './producer';
import { Receiver } from './receiver';

async function main() {
  const connection = await amqp.connect('amqp://guest:guest@localhost:5672');

  const routingKeys = ['black', 'white', 'red'];
  const exchange = 'task5';

  const channel = await connection.createChannel();
  /*
   * Создаем обменник типа direct который будет распределять сообщения в очереди с указанным ключом
   * */
  await channel.assertExchange(exchange, 'direct', { durable: false });

  const producer = new Producer(connection, routingKeys, exchange);
  const receiver1 = new Receiver(
    connection,
    [routingKeys[0], routingKeys[1]], // black + white
    exchange,
    1,
    1,
  );
  const receiver2 = new Receiver(
    connection,
    [routingKeys[2]], // red
    exchange,
    2,
    1,
  );

  await producer.startProducingToExchange(); // начнет генерировать сообщения каждые 2 секунды

  await receiver1.startReceive();
  await receiver2.startReceive();
}

main()
  .then()
  .catch((e) => console.log('Error', e));
