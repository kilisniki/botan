import * as amqp from 'amqplib';

export class Producer {
  private timerId: NodeJS.Timer;
  private messageId = 0;

  constructor(
    private readonly connection: amqp.Connection,
    public readonly queue: string,
  ) {}

  public async produce() {
    const channel = await this.connection.createChannel();

    console.log(' [P] Start producing...');

    for (let i = 0; i < 10; i++) {
      const message = ` [M] : ${++this.messageId}`;
      console.log(' [P] Send new message ', this.messageId);
      channel.sendToQueue(this.queue, Buffer.from(message));
    }
  }

  public async startProducing() {
    const channel = await this.connection.createChannel();
    await channel.assertQueue(this.queue, {
      durable: false,
    });

    console.log(' [x] Start producing...');

    this.timerId = setInterval(() => {
      for (let i = 0; i < 5; i++) {
        const message = ` [M] : ${++this.messageId}`;
        console.log(' [P] Send new message ', this.messageId);
        channel.sendToQueue(this.queue, Buffer.from(message));
      }
    }, 2000);
  }
}
