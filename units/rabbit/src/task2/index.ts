import * as amqp from 'amqplib';
import { Producer } from './producer';
import { Receiver } from './receiver';

async function main() {
  const connection = await amqp.connect('amqp://guest:guest@localhost:5672');
  const queue = 'task2';

  const channel = await connection.createChannel();
  await channel.assertQueue(queue, {
    durable: false,
  });

  const producer = new Producer(connection, queue);
  const receiver1 = new Receiver(connection, queue, 1);
  const receiver2 = new Receiver(connection, queue, 2);

  await producer.produce(); // сгенерирует 10 сообщений
  /*
   * Так как у нас prefetch = 0 (дефолтный), то все сообщения разберет
   * !!!первый консюмер !!!
   * */
  await receiver1.startReceive();
  await receiver2.startReceive();
  await producer.produce(); // сгенерирует 10 сообщений
  /*
   * Вторые 10 сообщений и последующие уже обоим достанутся, при чем
   * распределяться будет сразу вся пачка
   * */
  await producer.startProducing();
}

main()
  .then()
  .catch((e) => console.log('Error', e));
