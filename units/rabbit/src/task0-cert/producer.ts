import * as amqp from 'amqplib';
import { promises as fs } from 'fs';

async function main() {
  const connection = await amqp.connect(
    'amqp://guest:guest@domain.com:5671',
    /*{
      ca: [await fs.readFile('./cert.pem')],
    },*/
  );
  const queue = 'hello';
  const msg = 'Hello world';

  const channel = await connection.createChannel();
  await channel.assertQueue(queue, {
    durable: false, // очередь удалится брокером при перезагрузке реббита
    // exclusive: true // К очереди можно подключить только одно соединение (connection). То есть для самообслуживания
    // autoDelete: true // очередь удалится после того как последний консюмер отконнектится
    // arguments: [?] features such as message TTL, queue length limit, описаны в ноушене
  });

  console.log(' [P] Sending %s to %s', msg, queue);
  await channel.sendToQueue(queue, Buffer.from(msg));

  // Если не закрывать соединения, то программа завершается быстрее чем
  // сообщение попадает в реббит, очень интересная штука, не понял как это работает
  await channel.close();
  await connection.close();
}

main()
  .then(() => {
    console.log(' [P] Done!');
    process.exit(0);
  })
  .catch((e) => {
    console.log(' [P] Error!', e);
    process.exit(1);
  });
