import * as amqp from 'amqplib';

async function main() {
  const connection = await amqp.connect('amqp://guest:guest@domain.com/rabbit');
  const channel = await connection.createChannel();
  const queue = 'hello';

  // подключаемся к очереди
  await channel.consume(
    queue,
    (msg) => {
      console.log(' [R] Received %s', msg.content.toString());
      // connection.close();
      // process.exit(0);
    },
    {
      noAck: true, // автоматическое подтверждение https://www.rabbitmq.com/confirms.html
    },
  );
}
main()
  .then(() => {
    console.log(' [R] Connected, waiting messages...');
  })
  .catch((e) => {
    console.log(' [R] Error:', e);
  });
