import { map, Observable, of, pipe } from 'rxjs';

console.log('Иди ...');

// Pipe возвращает новый Observable
// поэтому мы можем сделать несколько цепочек на подпись
const observable = new Observable((subscriber) => {
  subscriber.next(1);
  subscriber.next(2);
  subscriber.next(3);
  setTimeout(() => {
    subscriber.next(4);
    subscriber.complete();
  }, 1000);
});

const chain1 = observable
  .pipe(
    map((value) => {
      console.log('map1, value:', value);
      return { value };
    }),
    map((value) => {
      console.log('map2, value:', value);
      return { value };
    }),
  )
  .subscribe((v) => {
    console.log('... chain1');
  });
const chain2 = observable
  .pipe(
    map((value) => {
      console.log('map3, value:', value);
      return { value };
    }),
  )
  .subscribe((v) => {
    console.log('... chain 2');
  });

/*
* OUTPUT
*
Иди ...
map1, value: 1
map2, value: { value: 1 }
... chain1
map1, value: 2
map2, value: { value: 2 }
... chain1
map1, value: 3
map2, value: { value: 3 }
... chain1
map3, value: 1
... chain 2
map3, value: 2
... chain 2
map3, value: 3
... chain 2
map1, value: 4
map2, value: { value: 4 }
... chain1
map3, value: 4
... chain 2

*
* */
