import { map, Observable, of, pipe, lastValueFrom, from } from 'rxjs';

(async () => {
  // Превращение Observable в Promise
  console.log('Иди ...');

  const observable = of(1, 2, 3, 4);

  const result = await observable.toPromise();
  console.log('obs1', result);

  const observable2 = of(1, 2, 3, 4);

  const result2 = await lastValueFrom(observable2);
  console.log('obs2', result2);

  // Превращение Promise в Observable
  const promise = new Promise((res, rej) => {
    setTimeout(() => res('Hello from Promise'));
  });

  const observable3 = from(promise);
  observable3.subscribe((value) => {
    console.log('obs3', value);
  });
})();
