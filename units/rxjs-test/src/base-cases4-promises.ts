import { map, Observable, of, pipe, lastValueFrom, from } from 'rxjs';

async function kek() {
  return new Promise((res) => setTimeout(res));
}

(async () => {
  // Превращение Observable в Promise
  console.log('Иди ...');

  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  const obs1 = new Observable(async (subscriber) => {
    await kek();
  });
})();
