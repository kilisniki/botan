import { map, Observable, of, pipe } from 'rxjs';

console.log('Иди ...');

// Простейший тест на то, в каком порядке выполнятся пайпы
const observable = new Observable((subscriber) => {
  subscriber.next(1);
  subscriber.next(2);
  subscriber.next(3);
  setTimeout(() => {
    subscriber.next(4);
    subscriber.complete();
  }, 1000);
})
  .pipe(
    map((value) => {
      console.log('map1, value:', value);
      return { value };
    }),
    map((value) => {
      console.log('map2, value:', value);
      return { value };
    }),
  )
  .pipe(
    map((value) => {
      console.log('map3, value:', value);
      return { value };
    }),
  )
  .subscribe((v) => {
    console.log('... нахер');
  });

// Почти как предыдущий пример, создаст Observable, но 4 элемент запушит сразу
of(1, 2, 3, 4)
  .pipe(
    map((value) => {
      console.log('map1, value:', value);
      return { value };
    }),
    map((value) => {
      console.log('map2, value:', value);
      return { value };
    }),
  )
  .pipe(
    map((value) => {
      console.log('map3, value:', value);
      return { value };
    }),
  )
  .subscribe((v) => {
    console.log('... к черту');
  });

// OUTPUT:
/*
Иди ...
map1, value: 1
map2, value: { value: 1 }
map3, value: { value: { value: 1 } }
... нахер
map1, value: 2
map2, value: { value: 2 }
map3, value: { value: { value: 2 } }
... нахер
map1, value: 3
map2, value: { value: 3 }
map3, value: { value: { value: 3 } }
... нахер
map1, value: 1
map2, value: { value: 1 }
map3, value: { value: { value: 1 } }
... к черту
map1, value: 2
map2, value: { value: 2 }
map3, value: { value: { value: 2 } }
... к черту
map1, value: 3
map2, value: { value: 3 }
map3, value: { value: { value: 3 } }
... к черту
map1, value: 4
map2, value: { value: 4 }
map3, value: { value: { value: 4 } }
... к черту
map1, value: 4
map2, value: { value: 4 }
map3, value: { value: { value: 4 } }
... нахер

*
* */
