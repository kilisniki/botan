# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.1.1](https://gitlab.com/kilisniki/botan/compare/v1.1.0-beta...v1.1.1) (2022-04-15)


### Bug Fixes

* add ts-ignore ([77cc3a1](https://gitlab.com/kilisniki/botan/commit/77cc3a16261ab73e3906f96fa591e75bdb126c5c))

## [1.1.0](https://gitlab.com/kilisniki/botan/compare/v0.0.4...v1.1.0) (2022-04-14)


### Features

* change text ([2f223a8](https://gitlab.com/kilisniki/botan/commit/2f223a8f77053f74f2b9ef35723085b5cec615b4))
