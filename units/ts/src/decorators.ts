/* eslint-disable @typescript-eslint/ban-types */
import 'reflect-metadata';
import {
  plainToClass,
  Transform,
  TransformFnParams,
  Expose,
  instanceToInstance,
  plainToInstance,
} from 'class-transformer';

export function Column(
  key: string,
  transform?: (value: string) => unknown,
): PropertyDecorator {
  console.log('column');
  return function f(target: unknown, propertyKey: string | symbol) {
    Reflect.defineMetadata('str', 'lop', target);
    Object.defineProperty(target, propertyKey, {
      get: function () {
        const value = this['__row'].getCell(key).text;
        if (transform) return transform(value);
        return value;
      },
      // eslint-disable-next-line @typescript-eslint/no-empty-function
      set: function () {
        console.log('set');
      },
    });
    return target;
  };
}

export function Column2(key: string): PropertyDecorator {
  return Transform((value: TransformFnParams) => {
    console.log(value);
    value.obj.getCell(key).text;
  });
}

function rowToClass<T>(constructor: new () => T, row): T {
  const mapped = {};
  const metadata: Map<string, string> = Reflect.getMetadata(
    'cell:mapping',
    constructor,
  );
  for (const [key, cellKey] of metadata.entries()) {
    mapped[key] = row.getCell(cellKey).text;
  }
  return plainToInstance(constructor, mapped);
}

export function Mapping(): ClassDecorator {
  return function f(target: unknown) {
    console.log(typeof target);
  };
}

export function Column3(key: string): PropertyDecorator {
  return function (target: object, propertyKey: string | symbol) {
    console.log('add metadata');
    let metadata = Reflect.getMetadata('cell:mapping', target.constructor);
    if (!metadata) {
      metadata = new Map();
      Reflect.defineMetadata('cell:mapping', metadata, target.constructor);
    }
    if (!metadata.get(propertyKey)) {
      metadata.set(propertyKey, key);
    }
  };
}

const row = {
  getCell: (key: string) => {
    return { text: `cellKey: ${key}` };
  },
};

class Test {
  @Column3('A')
  @Transform((val) => {
    console.log(val);
    return val.value + '1';
  })
  field: string;
}

console.log('start');
const a = rowToClass(Test, row);
console.log(a);

// const k = new Test();
// const l: Test = {
//   field: '  sdf   ',
//   //__row: row,
// };
//
// let k2, l2;
// try {
//   k2 = instanceToInstance(k);
// } catch (e) {
//   console.log(e);
// }
//
// // try {
// //   l2 = plainToClass(Test, l);
// // } catch (e) {
// //   console.log(e);
// // }
// console.log(k.field);
// console.log(k2.field);
// // console.log(l.field);
// // console.log(l2.field);
//
