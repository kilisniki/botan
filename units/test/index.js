/**
 * Кандидату надо реализовать функцию groupItems.
 *
 * У функции 1 входной параметр:
 * 1. items - массив объектов, который надо привести к сгруппированному представлению
 *
 * элемент массива состоит из 2-х полей:
 * 1. type - поле по которому надо группировать
 * 2. line - целочисленное число
 *
 * При группировке по type надо сделать следующие действия:
 * 1. убрать дубли
 * 2. сгруппировать подряд идущие line, например
 * если у одного типа есть line 6, 7, 8
 * надо создать ключи startLine: 6, endLine: 8

 * если line отдельно стоящее значение, например 7
 * т.е. нет line со значением line - 1 или line + 1
 * надо создать ключ startLine: 7
 *
 * У функции выходной параметр объект -
 * ключ - это type,
 * значение - массив отсортированных объектов
 * {startLine} или {startLine, endLine}
 *
 * Пример использования
 groupItems([
 {type: "#1", line: 4},
 {type: "#2", line: 4},
 {type: "#3", line: 8},
 {type: "#3", line: 8},
 {type: "#3", line: 9},
 {type: "#3", line: 8},
 {type: "#2", line: 5},
 {type: "#2", line: 4},
 {type: "#1", line: 2},
 {type: "#2", line: 4},
 {type: "#1", line: 1},
 {type: "#3", line: 7},
 {type: "#3", line: 2}
 ]);
 *
 * Результат:
 {
    "#1": [{
      startLine: 1, endLine: 2
    }, {
      startLine: 4
    }],
    "#2": [{
      startLine: 4, endLine: 5
    }],
    "#3": [, {
      startLine: 2
    }, {
      startLine: 7, endLine: 9
    }]
  }
 */

function groupItems(items) {
  const map = new Map();
  const result = {};

  for (const item of items) {
    if (!map.get(item.type)) {
      map.set(item.type, new Set());
    }
    const set = map.get(item.type);
    set.add(item.line);
  }
  for (const key of map.keys()) {
    result[key] = Array
      .from(map.get(key))
      .sort()
      .map(el => {
        return {line: el }})
  }

  return result;
}

const r = groupItems([
  {type: "#1", line: 4},
  {type: "#2", line: 4},
  {type: "#3", line: 8},
  {type: "#3", line: 8},
  {type: "#3", line: 9},
  {type: "#3", line: 8},
  {type: "#2", line: 5},
  {type: "#2", line: 4},
  {type: "#1", line: 2},
  {type: "#2", line: 4},
  {type: "#1", line: 1},
  {type: "#3", line: 7},
  {type: "#3", line: 2}
]);

console.log('result', r);
