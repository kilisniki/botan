import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { TestTypeFromContract } from '@contract/botan';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): TestTypeFromContract {
    return {
      char: '1',
      str: this.appService.getHello(),
    };
  }
}
