# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.7](https://gitlab.com/kilisniki/botan/compare/v1.1.0-beta...v0.0.7) (2022-04-15)


### Features

* refactoring ([92fa46a](https://gitlab.com/kilisniki/botan/commit/92fa46a2ff8d5e87ba329cf457a3cfb71139fb85))


### Bug Fixes

* add ts-ignore ([77cc3a1](https://gitlab.com/kilisniki/botan/commit/77cc3a16261ab73e3906f96fa591e75bdb126c5c))
* lint ([8d0f4a2](https://gitlab.com/kilisniki/botan/commit/8d0f4a24e8f4d103361b7c9abd7ec000f82fd971))
* update package-lock.json ([bdf7e44](https://gitlab.com/kilisniki/botan/commit/bdf7e44f8c4259e19096a89fa84e087d4f6e498b))

### [0.0.6](https://gitlab.com/kilisniki/botan/compare/v1.1.0-beta...v0.0.6) (2022-04-15)


### Features

* refactoring ([92fa46a](https://gitlab.com/kilisniki/botan/commit/92fa46a2ff8d5e87ba329cf457a3cfb71139fb85))


### Bug Fixes

* add ts-ignore ([77cc3a1](https://gitlab.com/kilisniki/botan/commit/77cc3a16261ab73e3906f96fa591e75bdb126c5c))
* lint ([8d0f4a2](https://gitlab.com/kilisniki/botan/commit/8d0f4a24e8f4d103361b7c9abd7ec000f82fd971))
* update package-lock.json ([bdf7e44](https://gitlab.com/kilisniki/botan/commit/bdf7e44f8c4259e19096a89fa84e087d4f6e498b))

### [0.0.5](https://gitlab.com/kilisniki/botan/compare/v1.1.0-beta...v0.0.5) (2022-04-15)


### Features

* refactoring ([92fa46a](https://gitlab.com/kilisniki/botan/commit/92fa46a2ff8d5e87ba329cf457a3cfb71139fb85))


### Bug Fixes

* add ts-ignore ([77cc3a1](https://gitlab.com/kilisniki/botan/commit/77cc3a16261ab73e3906f96fa591e75bdb126c5c))
* lint ([8d0f4a2](https://gitlab.com/kilisniki/botan/commit/8d0f4a24e8f4d103361b7c9abd7ec000f82fd971))
* update package-lock.json ([bdf7e44](https://gitlab.com/kilisniki/botan/commit/bdf7e44f8c4259e19096a89fa84e087d4f6e498b))

### [0.0.4](https://gitlab.com/kilisniki/botan/compare/v0.0.3...v0.0.4) (2022-04-14)

### [0.0.3](https://gitlab.com/kilisniki/botan/compare/v0.0.2...v0.0.3) (2022-04-14)

### [0.0.2](https://gitlab.com/kilisniki/botan/compare/v0.0.2-beta...v0.0.2) (2022-04-14)


### Bug Fixes

* text ([0a6a280](https://gitlab.com/kilisniki/botan/commit/0a6a2805647cf45b88947b655611f11af86bebe6))
